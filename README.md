# 3D module with blender

All files and notes related to the 3D course given at [La Cambre](https://lacambre.be/).

## YEAR BOOK

See all projects here : https://lacambre3d2021.polymorph.cool/

## catalog

Browse object catalog here : http://frankiezafe.org/LC3D21/

## administration / communication:

course attendance table: [framacalc](https://lite.framacalc.org/9pvo-b3d_lacambre_2021)

pad with random notes: [framapad](https://annuel2.framapad.org/p/b3d_lacambre_2021-9pvo?lang=en)

video conference: [meet.jit.si](https://meet.jit.si/LC3D21)

twitch with screen capture: [twitch](https://www.twitch.tv/frankiezafe)

remises: [framacalc](https://lite.framacalc.org/9pvo-b3d_lacambre_2021_remise)

## modules (fr):

### 30 septembre:

**//01**: interface de blender 15 minutes

**//02**: modelisation (bases)

- importation d'objets
- manipulation d'objects: duplication, groupes, collections
- object mode / edit mode
- travail avec la géométrie: création de faces, division, extrusion
- modificateurs
- lowpoly

### 07 octobre:

**//03**: rendu
- textes
- caméras
- lumières
- matériaux simples
- eevee et cycle
- freestyle
 
### 15 octobre:

révision des bases: exercice de conception et de rendu

**//04**: photo réalisme
- cycle
- HDRIs
- shaders: matériaux et rendus avancés

### 29 octobre: semaine atypique [cancelled]

### 05 novembre: vacances [cancelled]

### 12 novembre:

**//05**: modelisation (advanced)

- présentation des différents outils de sculpting
- modificateurs
- utilisation des paths

**//06**: exportation

- formats de sorties
- export gtlf & threejs (web)

### 02 & 10 february:

**//07**: animation (bases)

- timeline et graph editor

### 17 february:

**//08**: animation squelettale

- rigging & skinning

### 24 february:

**//09**: physics simulation

- utilisation de la physique
- cloth

### 10 march:

**//10**: UV editing

- all UV editing methods
- rules and good practices

### 24 march:

**//11**: impression 3D

- addon 3D Print Toolbox
- export STL + verification dans MeshLab

### TBD

**//??**: physics simulation

- particles 
- softbodies
- fluid

**//??**: 3D scan

- à organiser avec le fablab

**//??**: camera tracking

**//??**: génératif & procédural
- geometry nodes
- python scripting 

## database

### Paloma Plana

- **aspirateur balais**
- **brouette**

### Clement Delepierre

- **clou**
- **pneu de tracteur**

### Amélie Tricaud

- tondeuse à gazon électrique
- ventilateur de table

### Anatole Mélot

- **stylo bille (type bic avec mécanisme d'ouverture au bout)**
- valise trolley avec 4 roues orientables

### Zélie Dierckx

- **écumoir**
- **boîte de conserve**

### Emma Bianchi

- **marguerite avec tige**
- **éponge avec un côté abrasif**

### Ewan Andrade Lopes

- **truelle**
- **appareil photo réflexe avec objectif court**

### Lune Jusseau

- lampadaire
- prise électrique mâle coudée

### Clémentine Bost

- **hachoir à steak manuel**
- **fourchette**

### Matthieu Dupille

- **robinet mitigeur**
- **vis à tête hexagonale**

### Gaïa Ingargiola

- **machine à laver**
- **volant de voiture avec klaxon central**

### Hyunju Park

- **coussin**
- **champignon de paris**

### Eve GARANGER

- **aubergine**
- **brosse à poils durs avec grand manche en bois**

### TOM RAMBAUD

- **porte intérieure avec poignée**
- **choux-fleur**

### William Denis

- plaque d'égoût
- enceinte à deux voies

### Hippolyte Lesseliers

- coupe-ongle
- clé à molette

### Antoine Schmitter

- **paire de lunettes de soleil**
- **colonne de temple grec style ionique**

### Justine Brissard

- pot de fleur en terre cuite
- vase en porcelaine cassé

### Kevin Maoukola

- botte en caoutchouc
- essuie-glace

### Andrea González

- **roue de vélo**
- **fer à repasser**

### Mancini Lolà

- **foreuse**
- **fauteuil anglais**

## resources:

### addons

- Display shortcuts: https://github.com/jayanam/shortcut_VUr

### tech

#### free ( and possibly libre )

- https://blendswap.com
- https://threejs.org
- http://polyhaven.com
- https://blendermada.com
- https://textures.com
- https://www.mixamo.com + https://substance3d.adobe.com/plugins/mixamo-in-blender/
- https://www.meshlab.net

#### non-free

- https://blendermarket.com

### scripting

- bpy module documentation: https://docs.blender.org/api/current/
- imal wiki: - https://wiki.imal.org/fuzzysearch/results/blender
- frankiezafe python forge: https://gitlab.com/frankiezafe/forge.python

### projects / documentation

- Universal Everything : https://www.universaleverything.com
- RANDOM ACCESS CHARACTER by Benjamin Vedrenne: http://www.glkitty.com/pages/rac.html
- The Blender-Brussels are a series of monthly work sessions with the aim of providing a regular gathering and knowledge sharing space for artists and developpers interested in Python scripting in the context of Blender. [github](https://blender-brussels.github.io)
- Shiv Integer is a bot making assemblage art for 3D printers. [thingiverse](https://www.thingiverse.com/shivinteger) / [presentation](https://www.plummerfernandez.com/shiv-integer/)
- Simone C Niquille, Avatardesign & Identitystrategy [website](https://technofle.sh/)
- Goodbye Uncanny Valley (2017), a documentary about evolution of 3D in recent years, by Alan Warburton [vimeo](https://vimeo.com/237568588)

- Official blender website: http://blender.org
- About [GPL license](https://www.gnu.org/licenses/gpl-3.0.en.html) and the 4 essential liberties of [GNU](https://www.gnu.org/philosophy/philosophy.html) *GNU's Not Unix!*
