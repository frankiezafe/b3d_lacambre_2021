#!/usr/bin/python3

import os

EXT = ".glb"
PREFIX = "gltf/"

files = os.listdir("gltf/")
files.sort()

gtlfs = []
for f in files:
	if f[-len(EXT):] == EXT:
		gtlfs.append( [ f, f[:-len(EXT)].replace('_', ' ').lower() ] )

f = open( 'gltf_list.json', 'w' )
f.write( '[' )
for i in range( 0, len(gtlfs) ):
	if i > 0:
		f.write( ',' )
	f.write( '["'+ PREFIX + gtlfs[i][0] + '","' + gtlfs[i][1] + '"]' )
f.write( ']' )
f.close()
