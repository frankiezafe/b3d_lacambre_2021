import random, time

students = [
	'Paloma Plana',
	'Clement Delepierre',
	'Amélie Tricaud',
	'Anatole Mélot',
	'Zélie Dierckx',
	'Emma Bianchi',
	'Ewan Andrade Lopes',
	'Lune Jusseau',
	'Clémentine Bost',
	'Matthieu Dupille',
	'Gaïa Ingargiola',
	'Hyunju Park',
	'Eve GARANGER',
	'TOM RAMBAUD',
	'William Denis',
	'Hippolyte Lesseliers',
	'Antoine Schmitter',
	'Justine Brissard',
	'Kevin Maoukola',
	'Andrea González',
	'Mancini Lolà'
]

objects = [
	'fourchette',
	'écumoir',
	'coussin',
	'fauteuil anglais',
	'lampadaire',
	'hachoir à steak manuel',
	'plaque d\'égoût',
	'pneu de tracteur',
	'essuie-glace',
	'volant de voiture avec klaxon central',
	'roue de vélo',
	'coupe-ongle',
	'peigne',
	'brosse à poils durs avec grand manche en bois',
	'stylo bille (type bic avec mécanisme d\'ouverture au bout)',
	'pot de fleur en terre cuite',
	'machine à laver',
	'aspirateur balais',
	'tondeuse à gazon électrique',
	'fer à repasser',
	'ventilateur de table',
	'foreuse',
	'clé à molette',
	'vis à tête hexagonale',
	'maillet en bois',
	'choux-fleur',
	'clou',
	'truelle',
	'aubergine',
	'champignon de paris',
	'marguerite avec tige',
	'porte intérieure avec poignée',
	'fenêtre avec chassis en bois et croix centrale',
	'valise trolley avec 4 roues orientables',
	'prise électrique mâle coudée',
	'boîte de conserve',
	'enceinte à deux voies',
	'télévision à écran cathodique',
	'botte en caoutchouc',
	'éponge avec un côté abrasif',
	'vase en porcelaine cassé',
	'sac de piscine',
	'robinet mitigeur',
	'appareil photo réflexe avec objectif court',
	'brouette',
	'colonne de temple grec style ionique',
	'paire de lunettes de soleil',
]

# tirage au sort

print( "\nTIRAGE AU SORT\n##############\n" )

f = open( 'object_database.md', 'w' )
for student in students:
	f.write( '\n### ' + student + '\n\n' )
	objs = ''
	for o in range(0,2):
		i = int( random.randrange( 0, len(objects) ) ) % len(objects)
		obj = objects.pop(i)
		objs += '\t- ' + obj + '\n'
		f.write( '- ' + obj + '\n' )
	print( student, '\n', objs )
	time.sleep(1)
f.close()
