
### Paloma Plana

- aspirateur balais
- brouette

### Clement Delepierre

- clou
- pneu de tracteur

### Amélie Tricaud

- tondeuse à gazon électrique
- ventilateur de table

### Anatole Mélot

- stylo bille (type bic avec mécanisme d'ouverture au bout)
- valise trolley avec 4 roues orientables

### Zélie Dierckx

- écumoir
- boîte de conserve

### Emma Bianchi

- marguerite avec tige
- éponge avec un côté abrasif

### Ewan Andrade Lopes

- truelle
- appareil photo réflexe avec objectif court

### Lune Jusseau

- lampadaire
- prise électrique mâle coudée

### Clémentine Bost

- hachoir à steak manuel
- fourchette

### Matthieu Dupille

- robinet mitigeur
- vis à tête hexagonale

### Gaïa Ingargiola

- machine à laver
- volant de voiture avec klaxon central

### Hyunju Park

- coussin
- champignon de paris

### Eve GARANGER

- aubergine
- brosse à poils durs avec grand manche en bois

### TOM RAMBAUD

- porte intérieure avec poignée
- choux-fleur

### William Denis

- plaque d'égoût
- enceinte à deux voies

### Hippolyte Lesseliers

- coupe-ongle
- clé à molette

### Antoine Schmitter

- paire de lunettes de soleil
- colonne de temple grec style ionique

### Justine Brissard

- pot de fleur en terre cuite
- vase en porcelaine cassé

### Kevin Maoukola

- botte en caoutchouc
- essuie-glace

### Andrea González

- roue de vélo
- fer à repasser

### Mancini Lolà

- foreuse
- fauteuil anglais
