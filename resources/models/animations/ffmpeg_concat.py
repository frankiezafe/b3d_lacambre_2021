#!/usr/bin/python3

'''
ffmpeg -i render_anim_001.mp4 -i render_anim_002.mp4 -i render_anim_003.mp4 \
-filter_complex "[0:v] [1:v] [2:v] concat=n=3:v=1 [v]" \
-map "[v]" mergedVideo.mp4 
'''

import os

NEEDLE = "render_anim_"
EXT = ".mp4"
EXPORT = "final.mp4"

files = os.listdir(".")
files.sort()

videos = []
for f in files:
	if f[:len(NEEDLE)] == NEEDLE and f[-len(EXT):] == EXT:
		videos.append( f )

if len(videos) < 2:
	exit()

inputs = ""
filter_args = ""
for i in range( 0, len(videos) ):
	inputs += "-i " + videos[i] + " "
	filter_args += "[" + str(i) + ":v] "
cmd = "ffmpeg " + inputs
cmd += "-filter_complex \"" + filter_args + "concat=n=" + str(len(videos)) + ":v=1 [v] \" "
cmd += "-map \"[v]\" " + EXPORT

print( cmd )
os.system( cmd )

