import bpy, math, mathutils, random

TMPL = bpy.data.collections['gear_template']
FACTORY = bpy.data.collections['factory']
GEARS = bpy.data.collections['gears']
DEST = 'gears'

CONTEXT = bpy.context
SCENE = bpy.context.scene

GRID = True


# utils to copy collection
def copy_objects(from_col, to_col, linked):
    for o in from_col.objects:
        dupe = o.copy()
        if not linked and o.data:
            dupe.data = dupe.data.copy()
        to_col.objects.link(dupe)

def copy(parent, collection, linked=True):
    # creation of a new collection
    #cc = bpy.data.collections.new(collection.name)
    # specify the target collection 
    copy_objects(collection, FACTORY, linked)
    for c in collection.children:
        copy(FACTORY, c, linked)
    # required if a new collection is created
    #parent.children.link(cc)

def float_rand( _range, mult = 1 ):
    return ( _range[0] + random.random() * ( _range[1] - _range[0] ) ) * mult

def int_rand( _range, mult = 1 ):
    return int( ( _range[0] + random.random() * ( _range[1] - _range[0] ) ) * mult )

class gear:
    
    basis = None
    hole = None
    tooth_negative = None
    
    diameter = 1
    hole_height = 1
    hole_diameter = 0.3
    hole_union = False
    teeth_depth = 0.05
    teeth_count = 18
    teeth_mid = 0.7
    teeth_end = 0.5
    
    def __init__(self):
        return
    
    def select( self, o ):
        bpy.ops.object.select_all(action='DESELECT')
        o.select_set(True)
        bpy.context.view_layer.objects.active = o
    
    def delete(self, o):
        bpy.ops.object.select_all(action='DESELECT')
        o.select_set(True)
        bpy.ops.object.delete()
    
    # duplicate all template objects and generates a new mesh
    def generate(self):
        
        copy( SCENE.collection, TMPL, False )
        
        for o in FACTORY.objects:
            if o.name.startswith( 'basis' ):
                self.basis = o
            elif o.name.startswith( 'hole' ):
                self.hole = o
            elif o.name.startswith( 'tooth_negative' ):
                self.tooth_negative = o
        
        if self.basis == None or self.hole == None or self.tooth_negative == None:
            print( "NOT GOOD TO GO!" )
            return
        
        # relinking booleans
        tooth_bool = self.basis.modifiers['tooth']
        tooth_bool.object = self.tooth_negative
        center_bool = self.basis.modifiers['center']
        center_bool.object = self.hole
        if self.hole_union:
            center_bool.operation = 'UNION'
        
        # dimensions
        self.basis.scale.x = self.diameter * 0.5
        self.basis.scale.y = self.diameter * 0.5
        self.hole.scale.x = self.hole_diameter * 0.5
        self.hole.scale.y = self.hole_diameter * 0.5
        self.hole.scale.z = self.hole_height
        
        agap = math.pi * 2 / self.teeth_count
        x = math.cos( agap*0.25 )
        y = math.sin( agap*0.25 )
        normx = math.cos( agap*0.5 )
        normy = math.sin( agap*0.5 )
        
        mult = self.diameter * 0.5 - self.teeth_depth
        
        g_start_index = -1
        g_mid_index = -1
        g_end_index = -1
        
        for i in range( 0, len( self.tooth_negative.vertex_groups ) ):
            g = self.tooth_negative.vertex_groups[i]
            if g.name == 'start':
                g_start_index = i
            elif g.name == 'mid':
                g_mid_index = i
            elif  g.name == 'end':
                g_end_index = i
        
        tooth_mesh = self.tooth_negative.data
        for v in tooth_mesh.vertices:
            for vg in v.groups:
                # start
                if vg.group == g_start_index:
                    
                    v.co[0] = x * mult
                    if v.co[1] < 0:
                        v.co[1] = -y * mult
                    else:
                        v.co[1] = y * mult
                #  mid
                elif vg.group == g_mid_index:
                    v.co[0] = x * mult + normx * self.teeth_depth * self.teeth_mid
                    if v.co[1] < 0:
                        v.co[1] = -y * mult - normy * self.teeth_depth * self.teeth_mid
                    else:
                        v.co[1] = y * mult + normy * self.teeth_depth * self.teeth_mid
                # end
                elif vg.group == g_end_index:
                    mult = self.diameter * 0.5 - self.teeth_depth
                    v.co[0] = x * mult + self.teeth_depth + self.teeth_end
                    if v.co[1] < 0:
                        v.co[1] = -y * mult - self.teeth_end
                    else:
                        v.co[1] = y * mult + self.teeth_end
        
        self.select( self.basis )
        bpy.ops.object.modifier_apply( modifier='center' )
        bpy.ops.object.modifier_apply( modifier='tooth' )
        
        # now we can cut the teeth around basis
        for i in range( 1, self.teeth_count ):
            self.tooth_negative.rotation_euler.z = i * agap
            tname = name="tooth"+str(i)
            self.basis.modifiers.new( name=tname, type="BOOLEAN" )
            self.basis.modifiers[tname].object = self.tooth_negative
            self.select( self.basis )
            bpy.ops.object.modifier_apply( modifier = tname )
        
        self.delete( self.tooth_negative )
        self.delete( self.hole )
        
        self.select( self.basis )
        bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
        self.basis.name = 'gear_'+str(self.teeth_count)+'_'
        if self.hole_union:
            self.basis.name += 'axis_'+str(self.hole_diameter)
        else:
            self.basis.name += 'hole_'+str(self.hole_diameter)
        
        FACTORY.objects.unlink(self.basis)
        GEARS.objects.link(self.basis)

    def move( self, pos ):
        
        if self.basis == None:
            return
        
        self.basis.location = pos
    
    def rotate_z( self, rad ):
        
        if self.basis == None:
            return
        
        self.basis.rotation_euler.z = rad

# main #############

if GRID == False:
    
    g = gear()
    g.generate()

else:
    
    
    # config #1
    grid_dim = [5,5]
    r_diameter = [ 0.7, 2.5 ]
    r_hole_diameter = [ 0.1, 0.6 ]
    r_teeth_depth = [ 0.05, 0.2 ]
    r_teeth_count = [ 5, 40 ]
    # config #2
    '''
    grid_dim = [5,5]
    r_diameter = [ 0.5, 1 ]
    r_hole_diameter = [ 0.2, 0.35 ]
    r_teeth_depth = [ 0.05, 0.09 ]
    r_teeth_count = [ 10, 24 ]
    '''
    # config #3
    '''
    grid_dim = [5,5]
    r_diameter = [ 2, 2.5 ]
    r_hole_diameter = [ 0.4, 1.9 ]
    r_teeth_depth = [ 0.05, 0.09 ]
    r_teeth_count = [ 30, 50 ]
    '''
    # config #4
    '''
    grid_dim = [3,3]
    r_diameter = [ 4, 4.5 ]
    r_hole_diameter = [ 2, 3 ]
    r_teeth_depth = [ 0.05, 0.09 ]
    r_teeth_count = [ 80, 120 ]
    '''
    
    grid_offset = [ -grid_dim[0]+1, -grid_dim[1]+1 ]
    i = 0
    total = grid_dim[0] * grid_dim[1]
    
    for y in range( 0, grid_dim[1] ):
        for x in range( 0, grid_dim[0] ):
            # new gear
            g = gear()
            # randomisation
            g.diameter = float_rand( r_diameter )
            g.hole_diameter = float_rand( r_hole_diameter )
            g.teeth_depth = float_rand( r_teeth_depth )
            g.teeth_count = int_rand( r_teeth_count )
            # generation
            g.generate()
            g.move( ( grid_offset[0] + x*2, grid_offset[1] + y*2, 0 ) )
            g.rotate_z( random.random() * math.pi )
            
            print( i, '/', total )
            i += 1
