# characters incrustation in photograph

## base image

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/hotel-room-1395584662zbr.jpg)

[source](https://www.publicdomainpictures.net/en/view-image.php?image=79069&picture=hotel-room)

## camera positioning

### fSpy, open source still image camera matching 

- download and install: https://github.com/stuffmatic/fSpy/releases/tag/v1.0.3
- basics: https://fspy.io/basics/
- tutorial: https://fspy.io/

make sure Z axis pointing up!

save the project on your drive

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/001-fspy-camera.png)

### blender addons

- download: https://github.com/stuffmatic/fSpy-Blender/releases/download/v1.0.3/fSpy-Blender-1.0.3.zip
- install following these instructions: https://github.com/stuffmatic/fSpy-Blender

Once done, import the *.fspy project in blender

## room reconstruction

reconstruct the main volumes and place the main source lights

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/002-approximative-room-reconstruction.png)

## characters positioning

load your characters and place them in the space using pose mode, if your models have an armature

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/003-characters-positioning.png)

## rendering

make sure the room reconstruction is not visible in render by unchecking **camera** in **ray visibility** in every object you want to hide

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/004-invisible-in-render.png)

make sure your render have a transparent background (see screenshot)

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/005-export-RGBA_transparent_background.png)

Once rendered, the image should have just the characters.

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/hotel-room-1395584662zbr-over.png)

## mixing

with an image editor, import original image and render and mix them.

![](https://gitlab.com/frankiezafe/b3d_lacambre_2021/-/raw/master/examples/incrustation/hotel-room-1395584662zbr.-final.jpg)


