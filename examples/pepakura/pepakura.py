import bpy, math, mathutils, inspect

def common_vertices( poly0, poly1 ):
    verts = {
        'common': [],
        'local': [],
        'foreign': [],
    }
    for vid0 in poly0.vertices:
        if vid0 in poly1.vertices:
            verts['common'].append( vid0 )
        else:
            verts['local'].append( vid0 )
    for vid1 in poly1.vertices:
        if not vid1 in poly0.vertices:
            verts['foreign'].append( vid1 )
    return verts

def flat_polygon( mesh, poly, common ):
    # retrieving positions
    main_ids = []
    main = []
    other_id = None
    other = None
    for vid in poly.vertices:
        if vid in common:
            main_ids.append( vid )
            main.append( mesh.vertices[vid].co )
        else:
            other_id = vid
            other = mesh.vertices[vid].co
    # not good...
    if len( main ) != 2 or other == None:
        return False
    # computing edges
    main_edge = main[1] - main[0]
    main_length = main_edge.length
    main_edge /= main_length
    other_edge = other - main[0]
    # and using dot products to store relative positions
    om_dot = main_edge.dot( other_edge )
    om_dist = ( other_edge - main_edge * om_dot ).length
    
    # let's compute the perpendicular to main edge
    perp = main_edge.cross(poly.normal).normalized()
    # and check if other edge is opposite
    if perp.dot(other_edge) < 0:
        om_dist *= -1
    
    # generating vertices
    out = {
        "ids": [],
        "verts": []
    }
    # restarting the loop at 0,0,0
    startid = 0
    for vid in poly.vertices:
        if vid == main_ids[0]:
            break
        startid += 1
    # registration of the new vertex order
    for i in range(0,3):
        out['ids'].append( poly.vertices[(startid+i)%3] )
    # pushing data into verts whatr ever is the new vertex order
    for id in out["ids"]:
        if id == main_ids[0]:
            out["verts"].append( mathutils.Vector( (0,0,0) ) )
        elif id == main_ids[1]:
            out["verts"].append( mathutils.Vector( (main_length,0,0) ) )
        else:
            out["verts"].append( mathutils.Vector( (om_dot,om_dist,0) ) )
    return out

def align_flat_tri( src, dst ):
    
    pt0 = { 'local':-1, 'foreign':-1 }
    pt1 = { 'local':-1, 'foreign':-1 }
    
    # search common edge
    for si in range(0,3):
        for di in range(0,3):
            if src['ids'][si] == dst['ids'][di]:
                if pt0['local'] == -1:
                    pt0['local'] = si
                    pt0['foreign'] = di
                elif pt1['local'] == -1:
                    pt1['local'] = si
                    pt1['foreign'] = di
                else:
                    print("align_flat_tri: source and destination are equals!")
                    return
    
    e0 = (src['verts'][pt1['local']] - src['verts'][pt0['local']]).normalized()
    e1 = (dst['verts'][pt1['foreign']] - dst['verts'][pt0['foreign']]).normalized()
    pivot = dst['verts'][pt0['foreign']].copy()
    offset = src['verts'][pt0['local']]
    # computation of the 2 angles + mix
    ea = math.atan2(e0.y,e0.x) - math.atan2(e1.y,e1.x)
    eul = mathutils.Euler((0,0,ea),'XYZ')
    
    # rotation of the triangle + offset
    for vi in range(0,3):
        dst['verts'][vi] -= pivot
        dst['verts'][vi].rotate( eul )
        dst['verts'][vi] += offset
    
    '''
    # fucking complex debug!
    e1 = (dst['verts'][pt1['foreign']] - dst['verts'][pt0['foreign']]).normalized()
    print( "align_flat_tri" )
    print( '\t', pt0 )
    print( '\t', pt1 )
    print( '\t dot', e1.dot(e0) )
    print( '\t r0', r0 )
    print( '\t r1', r1 )
    print( '\t ea', ea )
    '''

def draw_flat_tri( pen, flat_p ):
    
    barycentre = (flat_p['verts'][0]+flat_p['verts'][1]+flat_p['verts'][2])/3
    
    layer = pen.data.layers.new("layer",set_active=True)
    frame = layer.frames.new(0)
    stroke = frame.strokes.new()
    
    stroke.display_mode = "3DSPACE"
    stroke.points.add(4)
    for i in range(4):
        stroke.points[i].co = flat_p['verts'][i%3]
    
    ''''
    stroke = frame.strokes.new()
    stroke.display_mode = "3DSPACE"
    stroke.points.add(4)
    for i in range(4):
       stroke.points[i].co =  flat_p['verts'][0] + mathutils.Vector(( math.cos(i*math.tau/3)*0.03, math.sin(i*math.tau/3)*0.03, 0 ))
    
    stroke = frame.strokes.new()
    stroke.display_mode = "3DSPACE"
    stroke.points.add(17)
    for i in range(17):
       stroke.points[i].co =  flat_p['verts'][1] + mathutils.Vector(( math.cos(i*math.tau/16)*0.03, math.sin(i*math.tau/16)*0.03, 0 ))
    '''

obj = bpy.context.view_layer.objects.active
mesh = obj.data
bpy.ops.object.mode_set(mode = 'OBJECT')

# retrieving candidates

candidates = []
for polygon in mesh.polygons:
    if polygon.select and len(polygon.vertices) == 3:
        candidates.append( polygon )

# building all connections

connections = []

for local in candidates:
    for foreign in candidates:
        if foreign == local:
            continue
        cv = common_vertices( local, foreign )
        if len(cv['common']) == 2:
            connections.append( {
                'local': local,
                'foreign': foreign,
                'verts': cv
            })

# building a sorted array of connected polygons

linked_polygons = []
needle = None
skip_list = []

loopcount = 0

while len( connections ) > 0:
    if len( linked_polygons ) == 0:
        # search the first face that only appear once in local
        for c in candidates:
            count = 0
            for cn in connections:
                if cn['local'] == c:
                    count += 1
            if count == 1:
                for cn in connections:
                    if cn['local'] == c:
                        skip_list.append( cn['local'] )
                        needle = cn['foreign']
                        linked_polygons.append( cn )
                        connections.remove( cn )
                        break
                break
        
        # cannot find a place to start
        # so unfolding starts at the first connection...
        if needle == None:
            cn = connections[0]
            local = cn['local']
            # removing all connection starting at local
            while local != None:
                found = False
                for c in connections:
                    if c['local'] == local:
                        found = True
                        connections.remove(c)
                        break
                if not found:
                    local = None 
            skip_list.append( cn['local'] )
            needle = cn['foreign']
            linked_polygons.append( cn )
            
    elif needle == None:
        connections = []
    else:
        found = False
        for cn in connections:
            if cn['local'] == needle and not cn['foreign'] in skip_list:
                found = True
                skip_list.append( cn['local'] )
                needle = cn['foreign']
                linked_polygons.append( cn )
                connections.remove( cn )
        if not found:
            needle = None
    
    print( "loopcount, ", loopcount )
    loopcount += 1
    
# starting output

pen=0
if "Flat_Exporter" not in bpy.context.scene.objects:
    bpy.ops.object.gpencil_add(location=(0,0,0),type="EMPTY")
    bpy.context.scene.objects[-1].name="Flat_Exporter"
pen=bpy.context.scene.objects["Flat_Exporter"]

previous_tri = None

count = 0

for lp in linked_polygons:
    
    if previous_tri == None:
        previous_tri = flat_polygon( mesh, lp['local'], lp['verts']['common'] )
        if previous_tri == False:
            print( "Error in drawing faces" )
            break
        draw_flat_tri( pen, previous_tri )
    
    flat_p = flat_polygon( mesh, lp['foreign'], lp['verts']['common'] )
    if flat_p == False:
        continue
    align_flat_tri( previous_tri, flat_p )
    draw_flat_tri( pen, flat_p )
    previous_tri = flat_p
    
    # usefull while debugging
    '''
    count += 1
    if count == 2:
        pass
    '''